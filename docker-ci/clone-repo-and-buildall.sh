#!/bin/sh

BUILD_OUTPUT_DIR=$HOME/termux-packages/debs

set -e

# Clone da repo and vaigate to it
git clone https://gitlab.com/termux-packages-android5x/src.git $HOME/termux-packages && cd $HOME/termux-packages

# Build all packages (excluding disabled ones)
$HOME/termux-packages -a all -i

# then tar them out
tar -zcvf $HOME/out/debs-build-dev.tar.gz $BUILD_OUTPUT_DIR
